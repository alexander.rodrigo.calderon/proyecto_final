package Modelo;


public class Login {
    int id;
    String dni;
    String nom;
    String user;
    String pass;

    public Login() {
    }

    public Login(int id, String dni, String nom, String user, String pass) {
        this.id = id;
        this.dni = dni;
        this.nom = nom;
        this.user = user;
        this.pass = pass;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
    
}
