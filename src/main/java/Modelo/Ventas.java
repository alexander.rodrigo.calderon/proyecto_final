package Modelo;

public class Ventas {
    int id;
    int idCliente;
    int idPresonal;
    String fecha;
    double Monto;
    String nroVentas;

    public Ventas() {
    }

    public Ventas(int id, int idCliente, int idPresonal, String fecha, double Monto, String nroVentas) {
        this.id = id;
        this.idCliente = idCliente;
        this.idPresonal = idPresonal;
        this.fecha = fecha;
        this.Monto = Monto;
        this.nroVentas = nroVentas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getIdPresonal() {
        return idPresonal;
    }

    public void setIdPresonal(int idPresonal) {
        this.idPresonal = idPresonal;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public double getMonto() {
        return Monto;
    }

    public void setMonto(double Monto) {
        this.Monto = Monto;
    }

    public String getNroVentas() {
        return nroVentas;
    }

    public void setNroVentas(String nroVentas) {
        this.nroVentas = nroVentas;
    }
    
    
}
