package Modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public class ProductosDAO implements CRUD{
    int r;
    PreparedStatement ps;
    ResultSet rs;
    Connection xcon;
    BaseDatos con = new BaseDatos();
    Productos pro = new Productos();

    
    public Productos ListarID(int id){
        Productos p = new Productos();
        String sql="select * from producto where IdProducto=?";
        try {
            xcon = con.Conectar();
            ps=xcon.prepareStatement(sql);
            ps.setInt(1, id);
            rs=ps.executeQuery();
            while (rs.next()){
                p.setId(rs.getInt(1));
                p.setNombre(rs.getString(2));
                p.setPrec(rs.getDouble(3));
                p.setStock(rs.getInt(4));
            }
        } catch (Exception e) {
        }
        return p;
    }
    
    public List listar() {
        List<Productos> lista = new ArrayList<>();
        String sql = "select * from producto";
        try {
            xcon = con.Conectar();
            ps=xcon.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()) {
                Productos pr = new Productos();
                pr.setId(rs.getInt(1));
                pr.setNombre(rs.getString(2));
                pr.setPrec(rs.getDouble(3));
                pr.setStock(rs.getInt(4));
                lista.add(pr);
            }
        } catch (Exception e) {
        }
        return lista;
    }

    @Override
    public void agregar(Object[] o){
        try {
            String xcod = con.generarCodigo("Producto","IdProducto");
            String xnom = (String) o[0];
            double xprec = (double) o[1];
            int xstock =  (int) o[2];
            String sql = "insert into producto (IdProducto, Nombre, Precio, Stock) values (?,?,?,?)";
            xcon = con.Conectar();
            ps=xcon.prepareStatement(sql);
            ps.setObject(1, xcod);
            ps.setObject(2, xnom);
            ps.setObject(3, xprec);
            ps.setObject(4, xstock);
            ps.executeUpdate();
            xcon.close();
        } catch (Exception e) {
            System.out.println("No se pudo agregar");
        }
    }

    @Override
    public void editar(Object[] o) {
        try {
            String xcod = (String) o[0];
            String xnom = (String) o[1];
            double xprec = (double) o[2];
            int xstock =  (int) o[3];
            String sql = "update producto set Nombre=?, Precio=?, Stock=? where IdProducto=?";
            xcon = con.Conectar();
            ps=xcon.prepareStatement(sql);
            ps.setObject(1, xnom);
            ps.setObject(2, xprec);
            ps.setObject(3, xstock);
            ps.setObject(4, xcod);
            ps.executeUpdate();
            xcon.close();
        } catch (Exception e) {
            System.out.println("No se pudo editar");
        }
    }

    @Override
    public void eliminar(int id) {
        String sql = "delete from producto where IdProducto=?";
        try {
            xcon = con.Conectar();
            ps=xcon.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }
}
