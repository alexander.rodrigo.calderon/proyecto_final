package Modelo;

import java.sql.SQLException;
import java.util.List;

public interface CRUD {
    public List listar();
    public void agregar(Object[] o);
    public void editar(Object[] o);
    public void eliminar(int id);
}
